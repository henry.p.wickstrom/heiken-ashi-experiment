//+------------------------------------------------------------------+
//|                                                 HeikenAshiEx.mq4 |
//|                                                         Henku Oy |
//+------------------------------------------------------------------+
#property strict
#property copyright "Henku Oy"
#property link      ""
#property version   "0.63"
string    version = "0.63";


input const double LOT_SIZE = 0.01;
input const double RISK_TO_REWARD_RATIO = 2.0;
input const int ADX_THRESHOLD = 20;
input const int MAX_SPREAD = 10;
input const int MAX_SLIPPAGE = 5;
input const int MAGIC_NUMBER = 4655424;


bool BUYS_ALLOWED;
bool SELLS_ALLOWED;
double UsePoint;
int UseSlippage;

static datetime NewCandleTime;

bool thisBarHasTradeOpen;
bool printedSpreadMessageForThisBar;

double previousHAOpen;
double previousHAClose;
bool previousCandleIsGreen;
double atr;


// Hooks
int OnInit(){
   UsePoint = getDecimalPip();
   UseSlippage = getSlippage();
   BUYS_ALLOWED = true;
   SELLS_ALLOWED = true;
   thisBarHasTradeOpen = false;
   printedSpreadMessageForThisBar = false;

   return(INIT_SUCCEEDED);
}

void OnDeinit(const int reason) {
   // ChartBackColorSet(Black, 0);
}

void OnTick() {
   RefreshRates();
   updateSignalData();
   displayInfo();

   // Avoid accidental opening of trade when starting EA
   if(NewCandleTime == NULL) {
      isNewBar();
      return;
   }

   if(isNewBar()) {
      thisBarHasTradeOpen = false;
      printedSpreadMessageForThisBar = false;
      previousCandleIsGreen = isUp(1);
   }
   
   if(thisBarHasTradeOpen) {
      Sleep(1000);
      return;
   }
   
   RefreshRates();
   if(isTradingCondition() && isBuySignal()) {
      placeTrade(OP_BUY);
   } else if(isTradingCondition() && isSellSignal()) {
      placeTrade(OP_SELL);
   }
   
   Sleep(200); 
   RefreshRates();
   updateStopLosses();
}


// Methods
bool isBuySignal() {
   bool priceBrokePreviousHAHigh = Ask > previousHAOpen && Ask > High[1];
   bool currentCandleIsGreen = isUp(0);

   return currentCandleIsGreen && priceBrokePreviousHAHigh && !previousCandleIsGreen;
}

bool isSellSignal() {
   bool priceBrokePreviousHALow = Bid < previousHAOpen && Bid < Low[1];
   bool currentCandleIsRed = !isUp(0);

   return currentCandleIsRed && priceBrokePreviousHALow && previousCandleIsGreen;
}

bool isTradingCondition() {
   return ADX_THRESHOLD < iADX(Symbol(), PERIOD_CURRENT, 14, PRICE_CLOSE, MODE_MAIN, 0);
   // return true; // For debugging
}

bool isSellCondition() {
   return false;
}

void updateStopLosses() {
   for(int i = 0; i < OrdersTotal(); i++) {
      if(!OrderSelect(i, SELECT_BY_POS, MODE_TRADES)) {
         Print("Failed to select order: " + (string)GetLastError());
         return;
      }

      if(OrderSymbol() == ChartSymbol() && OrderMagicNumber() == MAGIC_NUMBER) {
         RefreshRates();
         
         if(OrderType() == OP_BUY) {
            double existingStopLoss = OrderStopLoss();
            bool dynamicStopLoss = NormalizeDouble(Ask - atr * UsePoint, Digits);
            if(existingStopLoss < dynamicStopLoss) {
               bool orderModifyResult = OrderModify(OrderTicket(), OrderOpenPrice(), dynamicStopLoss, OrderTakeProfit(), 0, Blue);
               
               if(GetLastError() == 130 || GetLastError() == 0) {
                  // Broker doesn't allow placing SL at this market price
                  return;
               } else if(orderModifyResult) {
                  Print("SL modified - ATR: " + (string)atr + " - Previous SL: " + (string)existingStopLoss + " -> " + (string)dynamicStopLoss);
               } else {
                  Print("Error in OrderModify. Error code=", GetLastError());
               }
               
               Print("existingStopLoss: " + (string)existingStopLoss + ", dynamicStopLoss: " + (string)dynamicStopLoss + " -> " + (orderModifyResult ? "OK" : (string)GetLastError()));
            }
         } else if(OrderType() == OP_SELL) {
            double existingStopLoss = OrderStopLoss();
            bool dynamicStopLoss = NormalizeDouble(Bid + atr * UsePoint, Digits);
            if(existingStopLoss > dynamicStopLoss) {
               bool orderModifyResult = OrderModify(OrderTicket(), OrderOpenPrice(), dynamicStopLoss, OrderTakeProfit(), 0, Blue);
               
               
               if(GetLastError() == 130) {
                  // Broker doesn't allow placing SL at this market price
                  return;
               } else if(orderModifyResult) {
                  Print("SL modified - ATR: " + (string)atr + " - Previous SL: " + (string)existingStopLoss + " -> " + (string)dynamicStopLoss);
               } else {
                  Print("Error in OrderModify. Error code=", GetLastError());
               }
               
               Print("existingStopLoss: " + (string)existingStopLoss + ", dynamicStopLoss: " + (string)dynamicStopLoss + " -> " + (orderModifyResult ? "OK" : (string)GetLastError()));
            }
         }
      }
   }
}

void updateSignalData() {
   previousHAOpen = NormalizeDouble(iCustom(Symbol(), PERIOD_CURRENT, "Heiken Ashi", 2, 1), Digits);
   previousHAClose = NormalizeDouble(iCustom(Symbol(), PERIOD_CURRENT, "Heiken Ashi", 3, 1), Digits);
   atr = MathCeil((iATR(Symbol(), PERIOD_CURRENT, 10, 0) / UsePoint));
}

bool isUp(int shift) {
   const int LOW_HIGH_BUFFER = 0;
   const int HIGH_LOW_BUFFER = 1;
   const int OPEN_BUFFER = 2;
   const int CLOSE_BUFFER = 3;

   double lh = iCustom(Symbol(), PERIOD_CURRENT, "Heiken Ashi", LOW_HIGH_BUFFER, shift);
   double hl = iCustom(Symbol(), PERIOD_CURRENT, "Heiken Ashi", HIGH_LOW_BUFFER, shift);
   return lh < hl;
}

void displayInfo() {
   Comment(
      "HA Ex v. " + version +
      "\n\nTrading OK: " + (string)isTradingCondition() +
      // "\nAsk/Bid: " + (string)Ask+"/"+(string)Bid +
      "\nATR: " + (string)atr
   );
}

bool placeTrade(int type) {
   RefreshRates();

   int lotDigits = (int) - MathLog10(SymbolInfoDouble(Symbol(), SYMBOL_VOLUME_STEP));
   double lotSize = NormalizeDouble(LOT_SIZE, lotDigits);

   double spread = Ask - Bid;
   if(spread > MAX_SPREAD * UsePoint) {
      if(!printedSpreadMessageForThisBar) {
         Print("Spread too large, can't place trade: " + (string)spread);
         printedSpreadMessageForThisBar = true;
      }
      
      return(false);
   }

   int ticket = -1;
   if(type == OP_BUY) {
      double atrStopLoss = MathMax(10, atr);
      double takeProfit = NormalizeDouble(Bid + spread + atrStopLoss * RISK_TO_REWARD_RATIO * UsePoint, Digits);
      double suggestedStopLoss = NormalizeDouble(Bid - spread - atrStopLoss * UsePoint, Digits);
      double stopLoss = Low[1] < suggestedStopLoss ? Low[1] : suggestedStopLoss;
      Print(
         (type == OP_BUY ? "Buy Trade" : "Sell Trade") + " At " + (string)Bid +
         " - Trade Data - ADX: " + (string)iADX(Symbol(), PERIOD_CURRENT, 14, PRICE_CLOSE, MODE_MAIN, 0) +
         ", Prev Candle Green: " + (string)isUp(1) +
         ", High[1]:" + (string)High[1] +
         ", Low[1]:" + (string)Low[1] +
         ", ATR: " + (string)atr + " - Actual SL: " + (string)stopLoss +
         ", atrStopLoss: " + (string)atrStopLoss +
         ", takeProfit: " + (string)takeProfit
      );
      ticket = OrderSend(Symbol(), OP_BUY, lotSize, Ask, MAX_SLIPPAGE, stopLoss, takeProfit, "Spread Test", MAGIC_NUMBER, 0, Green);
   } else if(type == OP_SELL) {
      double atrStopLoss = MathMax(10, atr);
      double takeProfit = NormalizeDouble(Ask - spread - atrStopLoss * RISK_TO_REWARD_RATIO * UsePoint, Digits);
      double suggestedStopLoss = NormalizeDouble(Ask + spread - atrStopLoss * UsePoint, Digits);
      double stopLoss = High[1] > suggestedStopLoss ? High[1] : suggestedStopLoss;
      Print(
         (type == OP_BUY ? "Buy Trade" : "Sell Trade") + " At " + (string)Bid +
         " - Trade Data - ADX: " + (string)iADX(Symbol(), PERIOD_CURRENT, 14, PRICE_CLOSE, MODE_MAIN, 0) +
         ", Prev Candle Green: " + (string)isUp(1) +
         ", High[1]:" + (string)High[1] +
         ", Low[1]:" + (string)Low[1] +
         ", ATR: " + (string)atr + " - Actual SL: " + (string)stopLoss +
         ", atrStopLoss: " + (string)atrStopLoss +
         ", takeProfit: " + (string)takeProfit
      );
      ticket = OrderSend(Symbol(), OP_SELL, lotSize, Bid, MAX_SLIPPAGE, stopLoss, takeProfit, "Spread Test", MAGIC_NUMBER, 0, Green);
   }
   
   if(GetLastError() == 4110) {
      Print("Buys are not allowed. Buys will not be attempted from now on.");
      BUYS_ALLOWED = false;
      return false;
   } else if(GetLastError() == 4111) {
      Print("Sells are not allowed. Sells will not be attempted from now on.");
      SELLS_ALLOWED = false;
      return false;
   }

   int lastError = GetLastError();
   if(ticket < 0 && lastError > 0) {
      Print("Error(" + (string)ticket + "): " + (string)lastError);
      return(false);
   }
   
   thisBarHasTradeOpen = true;

   return(true);
}


// Common methods
double getDecimalPip() {
   switch(Digits) {
      case 5:
         return(0.0001);
      case 4:
         return(0.0001);
      case 3:
         return(0.001);
      default:
         return(0.01);
   }
}

bool isNewBar() {
   // Ensure correct result when starting robot
   if(NewCandleTime == NULL) {
      NewCandleTime = iTime(Symbol(),0,0);
      return false;
   }

   if(NewCandleTime == iTime(Symbol(),0,0)) {
      return false;
   } else {
      NewCandleTime = iTime(Symbol(),0,0);
      return true;
   }
}

int getSlippage() {
   if(Digits() == 2 || Digits() == 4) return(MAX_SLIPPAGE);
   else if(Digits() == 3 || Digits() == 5) return(MAX_SLIPPAGE * 10);
   return(Digits());
}
